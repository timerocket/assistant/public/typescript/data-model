## Deprecated

Please use one of the following instead

**Codex framework data model:**
https://gitlab.com/cryptexlabs/public/codex-data-model

**TimeRocket Assistant data model:**
https://gitlab.com/timerocket/assistant/public/typescript/assistant-data-model
