"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MetaTypeEnum = void 0;
var MetaTypeEnum;
(function (MetaTypeEnum) {
    MetaTypeEnum["RESPONSE_MESSAGE_ALEXA"] = "response.message.alexa";
    MetaTypeEnum["RESPONSE_MESSAGE_GOOGLE_VOICE"] = "response.message.google-voice";
    MetaTypeEnum["RESPONSE_MESSAGE_SIRI"] = "response.message.siri";
    MetaTypeEnum["RESPONSE_MESSAGE_MAC_OS"] = "response.message.mac-os";
    MetaTypeEnum["RESPONSE_MESSAGE_IOS"] = "response.message.ios";
    MetaTypeEnum["RESPONSE_MESSAGE_ANDROID"] = "response.message.android";
    MetaTypeEnum["RESPONSE_MESSAGE_SMS"] = "response.message.sms";
    MetaTypeEnum["RESPONSE_MESSAGE_MMS"] = "response.message.mms";
    MetaTypeEnum["REPLY_MESSAGE_MMS"] = "reply.message.mms";
    MetaTypeEnum["REPLY_MESSAGE_SMS"] = "reply.message.sms";
    MetaTypeEnum["REPLY_MESSAGE_APP"] = "reply.message.app";
    MetaTypeEnum["REPLY_MESSAGE_APP_UNASSIGNED_INTERACTION"] = "reply.message.app.unassigned-interaction";
    MetaTypeEnum["REPLY_MESSAGE_SMS_UNASSIGNED_INTERACTION"] = "reply.message.sms.unassigned-interaction";
    MetaTypeEnum["REPLY_MESSAGE_MMS_UNASSIGNED_INTERACTION"] = "reply.message.mms.unassigned-interaction";
    MetaTypeEnum["NA_HTTP_ERROR"] = "na.http.error";
    MetaTypeEnum["NA_HTTP_SIMPLE"] = "na.http.simple";
    MetaTypeEnum["NA_HTTP_HEALTHZ"] = "na.http.healthz";
    MetaTypeEnum["NA_WEBSOCKET_ERROR"] = "na.websocket.error";
    MetaTypeEnum["NA_WEBSOCKET_SIMPLE"] = "na.websocket.simple";
    MetaTypeEnum["NA_WEBSOCKET_SUBSCRIPTIONS_UPDATED"] = "na.websocket.subscriptions.updated";
    MetaTypeEnum["APP_MESSAGE_NEW_REPLY"] = "app.message.new-reply";
    MetaTypeEnum["APP_DATA"] = "app.data";
    MetaTypeEnum["SUPPORT_USER_INTERACTION"] = "support-user.interaction";
    MetaTypeEnum["AUTHENTICATION_TOKEN_PAIR"] = "authentication.token-pair";
})(MetaTypeEnum = exports.MetaTypeEnum || (exports.MetaTypeEnum = {}));
