import { AuthenticationProviderInterface } from "./authentication-provider.interface";
import { BasicAuthenticationDataInterface } from "./basic-authentication-data.interface";
import { AuthenticationProviderTypeEnum } from "./authentication-provider-type.enum";
export interface BasicAuthAuthenticationProviderInterface extends AuthenticationProviderInterface {
    data: BasicAuthenticationDataInterface;
    type: AuthenticationProviderTypeEnum.BASIC;
}
