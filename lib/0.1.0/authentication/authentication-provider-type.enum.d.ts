export declare enum AuthenticationProviderTypeEnum {
    BASIC = "basic",
    REFRESH = "refresh"
}
