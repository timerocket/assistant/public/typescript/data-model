export interface BasicAuthenticationDataInterface {
    username: string;
    password: string;
}
