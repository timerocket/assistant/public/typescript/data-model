import { AuthenticationTokenDataInterface } from "./authentication.token-data.interface";
import { AuthenticationProviderTypes } from "./authentication.provider.types";
export interface AuthenticationInterface {
    token: AuthenticationTokenDataInterface;
    providers: AuthenticationProviderTypes[];
}
