"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthenticationProviderTypeEnum = void 0;
var AuthenticationProviderTypeEnum;
(function (AuthenticationProviderTypeEnum) {
    AuthenticationProviderTypeEnum["BASIC"] = "basic";
    AuthenticationProviderTypeEnum["REFRESH"] = "refresh";
})(AuthenticationProviderTypeEnum = exports.AuthenticationProviderTypeEnum || (exports.AuthenticationProviderTypeEnum = {}));
