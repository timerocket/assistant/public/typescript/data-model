import { AttachmentInterface } from "./attachment.interface";
import { AttachmentTypeEnum } from "./attachment-type.enum";
export interface ImageAttachmentInterface extends AttachmentInterface {
    type: AttachmentTypeEnum.IMAGE;
}
