"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AttachmentTypeEnum = void 0;
var AttachmentTypeEnum;
(function (AttachmentTypeEnum) {
    AttachmentTypeEnum["VIDEO"] = "VIDEO";
    AttachmentTypeEnum["IMAGE"] = "IMAGE";
    AttachmentTypeEnum["DOCUMENT"] = "DOCUMENT";
})(AttachmentTypeEnum = exports.AttachmentTypeEnum || (exports.AttachmentTypeEnum = {}));
