import { AttachmentInterface } from "./attachment.interface";
import { AttachmentTypeEnum } from "./attachment-type.enum";
export interface DocumentAttachmentInterface extends AttachmentInterface {
    type: AttachmentTypeEnum.DOCUMENT;
}
