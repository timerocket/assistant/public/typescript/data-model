export declare enum ResponseDestinationTypeEnum {
    SMS = "SMS",
    EMAIL = "EMAIL",
    VOICE_ALEXA = "VOICE_ALEXA",
    VOICE_GOOGLE = "VOICE_GOOGLE",
    VOICE_APPLE = "VOICE_APPLE"
}
