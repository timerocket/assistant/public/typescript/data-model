import { SourceDestinationDeviceType } from "../../source-destination-device.type";
export interface ResponseDestinationInterface {
    device: SourceDestinationDeviceType;
    userId: string;
}
