import { AppAndroidDevice } from "../../app/app-android-device";
export interface AndroidAppDestinationInterface {
    device: AppAndroidDevice;
}
