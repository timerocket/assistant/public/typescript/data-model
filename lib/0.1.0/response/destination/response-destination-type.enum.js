"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseDestinationTypeEnum = void 0;
var ResponseDestinationTypeEnum;
(function (ResponseDestinationTypeEnum) {
    ResponseDestinationTypeEnum["SMS"] = "SMS";
    ResponseDestinationTypeEnum["EMAIL"] = "EMAIL";
    ResponseDestinationTypeEnum["VOICE_ALEXA"] = "VOICE_ALEXA";
    ResponseDestinationTypeEnum["VOICE_GOOGLE"] = "VOICE_GOOGLE";
    ResponseDestinationTypeEnum["VOICE_APPLE"] = "VOICE_APPLE";
})(ResponseDestinationTypeEnum = exports.ResponseDestinationTypeEnum || (exports.ResponseDestinationTypeEnum = {}));
