import { VoiceEchoDevice } from "../../voice";
export interface EchoResponseDestinationInterface {
    device: VoiceEchoDevice;
}
