import { CallToActionInterface } from "../cta";
import { ResponseDestinationInterface } from "../destination";
import { InteractionDataInterface } from "../../interaction";
import { MessageTypeEnum } from "../../message";
import { AppMessageDataType } from "../../app";
export interface ResponseDataInterface extends InteractionDataInterface {
    text: string;
    ctas: CallToActionInterface[];
    destination: ResponseDestinationInterface;
    messageId: string;
    type: MessageTypeEnum;
    data?: AppMessageDataType;
}
