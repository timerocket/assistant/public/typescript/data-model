import { ResponseDataInterface } from "./response-data.interface";
import { AttachmentInterface } from "../attachment";
export interface EmailResponseDataInterface extends ResponseDataInterface {
    attachments: AttachmentInterface[];
}
