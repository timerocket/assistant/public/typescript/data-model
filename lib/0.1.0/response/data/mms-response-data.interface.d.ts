import { ResponseDataInterface } from "./response-data.interface";
import { ImageAttachmentInterface, VideoAttachmentInterface } from "../attachment";
export interface MmsResponseDataInterface extends ResponseDataInterface {
    attachments: ImageAttachmentInterface | VideoAttachmentInterface[];
}
