import { CallToActionInterface } from "./call-to-action.interface";
export interface InteractiveCallToActionInterface extends CallToActionInterface {
    text: string;
    optionId: string;
    imageURL: string;
}
