import { CallToActionTypeEnum } from "./call-to-action-type.enum";
export interface CallToActionInterface {
    type: CallToActionTypeEnum;
}
