import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
import { Device } from "../device";
export interface PhoneDevice extends Device {
    phoneNumber: string;
    type: SourceDestinationTypeEnum.PHONE_MMS | SourceDestinationTypeEnum.PHONE_SMS;
}
