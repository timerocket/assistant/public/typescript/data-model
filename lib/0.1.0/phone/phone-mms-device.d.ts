import { PhoneDevice } from "./phone-device";
import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
export interface PhoneMmsDevice extends PhoneDevice {
    type: SourceDestinationTypeEnum.PHONE_MMS;
}
