export declare enum AppMessageTypeEnum {
    TEXT = "text",
    CARD = "card",
    CARD_LIST = "card.list"
}
