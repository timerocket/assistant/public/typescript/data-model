import { MessageTypeEnum } from "../message";
export interface AppMessageInterface {
    id: string;
    text: string;
    type: MessageTypeEnum;
    user: {
        id: 1 | 2;
    };
    createdAt: string;
}
