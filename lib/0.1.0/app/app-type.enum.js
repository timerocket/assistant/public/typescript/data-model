"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppTypeEnum = void 0;
var AppTypeEnum;
(function (AppTypeEnum) {
    AppTypeEnum["IOS"] = "ios";
    AppTypeEnum["ANDROID"] = "android";
    AppTypeEnum["ECHO"] = "echo";
})(AppTypeEnum = exports.AppTypeEnum || (exports.AppTypeEnum = {}));
