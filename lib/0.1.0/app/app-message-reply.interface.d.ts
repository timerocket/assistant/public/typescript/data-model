import { AppMessageInterface } from "./app-message.interface";
export interface AppMessageReplyInterface extends AppMessageInterface {
    user: {
        id: 2;
    };
}
