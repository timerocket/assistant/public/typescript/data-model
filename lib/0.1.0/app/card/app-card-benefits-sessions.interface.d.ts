import { SavedCardInterface } from "./app-message-card-saved.interface";
export interface AppCardBenefitsSessionsInterface extends SavedCardInterface {
    title: string;
    description: string;
    sessionCount: number;
    sessionsComplete: number;
}
