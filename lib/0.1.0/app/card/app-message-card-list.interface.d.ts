import { AppMessageCardInterface } from "./app-message-card-interface";
import { AppCardType } from "./app-card-type";
export interface AppMessageCardListInterface<T extends AppCardType> {
    data: AppMessageCardInterface<T>[];
}
