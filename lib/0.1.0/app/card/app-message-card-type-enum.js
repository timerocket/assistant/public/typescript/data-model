"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppMessageCardTypeEnum = void 0;
var AppMessageCardTypeEnum;
(function (AppMessageCardTypeEnum) {
    AppMessageCardTypeEnum["BENEFITS_SESSIONS"] = "card.benefit.session";
})(AppMessageCardTypeEnum = exports.AppMessageCardTypeEnum || (exports.AppMessageCardTypeEnum = {}));
