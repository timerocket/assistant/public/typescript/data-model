import { CommunicationType } from "./communication.type";
import { AppNavigationInterface } from "./navigation";
import { AppProfileInterface } from "./profile";
import { AppCardType, AppMessageCardInterface } from "./card";
export interface AppDataInterface {
    messages: CommunicationType[];
    communications: CommunicationType[];
    navigation: AppNavigationInterface[];
    profile: AppProfileInterface;
    cards: AppMessageCardInterface<AppCardType>[];
}
