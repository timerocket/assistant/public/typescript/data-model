"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppProfileSectionItemTypeEnum = void 0;
var AppProfileSectionItemTypeEnum;
(function (AppProfileSectionItemTypeEnum) {
    AppProfileSectionItemTypeEnum["INSURANCE_CARD"] = "user.profile.section.user-information.item.insurance-card";
    AppProfileSectionItemTypeEnum["TEXT"] = "user.profile.section.user-information.item.text";
})(AppProfileSectionItemTypeEnum = exports.AppProfileSectionItemTypeEnum || (exports.AppProfileSectionItemTypeEnum = {}));
