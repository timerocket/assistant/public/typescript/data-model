"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppNavigationNameEnum = void 0;
var AppNavigationNameEnum;
(function (AppNavigationNameEnum) {
    AppNavigationNameEnum["INSURANCE_CARD"] = "insurance-card";
})(AppNavigationNameEnum = exports.AppNavigationNameEnum || (exports.AppNavigationNameEnum = {}));
