"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppMessageTypeEnum = void 0;
var AppMessageTypeEnum;
(function (AppMessageTypeEnum) {
    AppMessageTypeEnum["TEXT"] = "text";
    AppMessageTypeEnum["CARD"] = "card";
    AppMessageTypeEnum["CARD_LIST"] = "card.list";
})(AppMessageTypeEnum = exports.AppMessageTypeEnum || (exports.AppMessageTypeEnum = {}));
