"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SourceDestinationTypeEnum = void 0;
var SourceDestinationTypeEnum;
(function (SourceDestinationTypeEnum) {
    SourceDestinationTypeEnum["PHONE_SMS"] = "PHONE_SMS";
    SourceDestinationTypeEnum["PHONE_MMS"] = "PHONE_MMS";
    SourceDestinationTypeEnum["EMAIL"] = "EMAIL";
    SourceDestinationTypeEnum["APP_IOS"] = "APP_IOS";
    SourceDestinationTypeEnum["APP_ANDROID"] = "APP_ANDROID";
    SourceDestinationTypeEnum["APP_WEB"] = "APP_WEB";
    SourceDestinationTypeEnum["VOICE_ECHO"] = "VOICE_ECHO";
    SourceDestinationTypeEnum["VOICE_GOOGLE"] = "VOICE_GOOGLE";
    SourceDestinationTypeEnum["VOICE_APPLE"] = "VOICE_APPLE";
})(SourceDestinationTypeEnum = exports.SourceDestinationTypeEnum || (exports.SourceDestinationTypeEnum = {}));
