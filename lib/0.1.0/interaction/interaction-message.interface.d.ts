import { MessageInterface } from "../message.interface";
import { InteractionDataInterface } from "./interaction-data.interface";
export interface InteractionMessageInterface<T extends InteractionDataInterface> extends MessageInterface<T> {
    data: T;
}
