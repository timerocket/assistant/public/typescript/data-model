"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Locale = void 0;
class Locale {
    constructor(language, country) {
        this.language = language;
        this.country = country;
    }
    get i18n() {
        return `${this.language}-${this.country}`;
    }
}
exports.Locale = Locale;
