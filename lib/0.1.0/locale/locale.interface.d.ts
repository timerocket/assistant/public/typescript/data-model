import { CountryCode, LanguageCode } from "./types";
export interface LocaleInterface {
    language: LanguageCode;
    country: CountryCode;
}
