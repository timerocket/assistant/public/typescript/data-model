import { CountryCode, LanguageCode } from "./types";
export interface LocaleI18nInterface {
    language: LanguageCode;
    country: CountryCode;
    readonly i18n: string;
}
