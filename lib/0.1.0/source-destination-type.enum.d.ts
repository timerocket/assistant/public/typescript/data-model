export declare enum SourceDestinationTypeEnum {
    PHONE_SMS = "PHONE_SMS",
    PHONE_MMS = "PHONE_MMS",
    EMAIL = "EMAIL",
    APP_IOS = "APP_IOS",
    APP_ANDROID = "APP_ANDROID",
    APP_WEB = "APP_WEB",
    VOICE_ECHO = "VOICE_ECHO",
    VOICE_GOOGLE = "VOICE_GOOGLE",
    VOICE_APPLE = "VOICE_APPLE"
}
