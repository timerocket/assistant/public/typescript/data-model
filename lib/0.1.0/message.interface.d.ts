import { MessageMetaInterface } from "./message-meta.interface";
export interface MessageInterface<T> {
    meta: MessageMetaInterface;
    data: T;
}
