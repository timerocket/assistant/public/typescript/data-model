import { AppSourceInterface, PhoneSourceInterface, VoiceSourceInterface } from "./source";
import { AppDevice } from "../app";
import { VoiceDevice } from "../voice/voice-device";
import { PhoneDevice } from "../phone";
export declare type ReplyDataType = PhoneSourceInterface<PhoneDevice> | AppSourceInterface<AppDevice> | VoiceSourceInterface<VoiceDevice>;
