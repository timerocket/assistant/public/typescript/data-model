import { InteractionDataInterface } from "../interaction";
import { InteractionUnassignedReplyDataInterface } from "./interaction-unassigned-reply-data.interface";
import { ReplyDataType } from "./reply-data.type";
export interface ReplyDataInterface<T extends ReplyDataType> extends InteractionUnassignedReplyDataInterface<T>, InteractionDataInterface {
}
