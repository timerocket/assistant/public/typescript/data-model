import { SourceInterface } from "./source.interface";
import { VoiceDevice } from "../../voice/voice-device";
export interface VoiceSourceInterface<T extends VoiceDevice> extends SourceInterface {
    device: T;
}
