import { SourceInterface } from "./source.interface";
import { AppDevice } from "../../app";
export interface AppSourceInterface<T extends AppDevice> extends SourceInterface {
    device: T;
    userId: string | null;
}
