import { PhoneNumberChannelEnum } from "./phone-number-channel.enum";
import { SourceInterface } from "./source.interface";
export interface PhoneNumberSourceInterface extends SourceInterface {
    sentToPhoneNumber: string;
    channel: PhoneNumberChannelEnum;
}
