import { ReplyDataType } from "./reply-data.type";
import { MessageTypeEnum } from "../message";
export interface InteractionUnassignedReplyDataInterface<T extends ReplyDataType> {
    source: T;
    text: string;
    messageId: string;
    type: MessageTypeEnum;
}
