import { SupportUserCommunication } from "./support-user.communication";
export interface SupportUserInteraction {
    id: string;
    communications: SupportUserCommunication[];
}
