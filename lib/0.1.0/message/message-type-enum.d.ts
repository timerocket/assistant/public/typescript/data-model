export declare enum MessageTypeEnum {
    TEXT = "text",
    CARD = "card",
    CARD_LIST = "card.list"
}
