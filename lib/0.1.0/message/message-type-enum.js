"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessageTypeEnum = void 0;
var MessageTypeEnum;
(function (MessageTypeEnum) {
    MessageTypeEnum["TEXT"] = "text";
    MessageTypeEnum["CARD"] = "card";
    MessageTypeEnum["CARD_LIST"] = "card.list";
})(MessageTypeEnum = exports.MessageTypeEnum || (exports.MessageTypeEnum = {}));
