import { MessageContextInterface } from "./message-context";
import { LocaleInterface } from "./locale";
import { MetaTimeInterface } from "./meta-time.interface";
import { MetaTypeEnum } from "./meta-type.enum";
import { ClientInterface } from "./client.interface";

export interface MessageMetaInterface {
  /**
   * Unique identifier generated when a person takes an action or a system time based event is triggered
   * This correlation id should be passed between systems and should be only created if none exists
   * Stops being re-used when an action is required from a person
   * Format is UUID V4
   */
  correlationId: string;

  /**
   * Time information about the message
   */
  time: MetaTimeInterface;

  /**
   * Every endpoint must be able to receive and pass along these properties
   */
  context: MessageContextInterface;

  /**
   * The system or application that generated the message
   */
  client: ClientInterface;

  /**
   * Unique canonical identifier to identify which model should be used for destructuring and handling of message
   */
  type: MetaTypeEnum;

  /**
   * Semantic version for message type. Identifies the version of schema that should be used to destructure the message
   * Should be same as package version
   */
  schemaVersion: "0.1.0";

  /**
   * Information regarding the language and region that the message is oriented for.
   */
  locale: LocaleInterface;
}
