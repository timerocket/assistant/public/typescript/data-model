import { CountryCode, LanguageCode } from "./types";
import { LocaleI18nInterface } from "./locale.i18n.interface";

export class Locale implements LocaleI18nInterface {
  constructor(public language: LanguageCode, public country: CountryCode) {}
  get i18n() {
    return `${this.language}-${this.country}`;
  }
}
