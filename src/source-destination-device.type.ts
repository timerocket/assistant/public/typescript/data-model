import { PhoneDevice } from "./phone";
import { AppDevice } from "./app";
import { VoiceDevice } from "./voice/voice-device";

export declare type SourceDestinationDeviceType =
  | PhoneDevice
  | AppDevice
  | VoiceDevice;
