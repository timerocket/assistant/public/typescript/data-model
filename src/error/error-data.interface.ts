import { ErrorMessageInterface } from "./error-message.interface";

export interface ErrorDataInterface {
  message: ErrorMessageInterface;
  stack: string;
}
