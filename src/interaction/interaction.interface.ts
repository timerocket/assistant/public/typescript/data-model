export interface InteractionInterface {
  /**
   * Unique identifier generated when an end user begins an interaction with us
   * This correlation id should be passed between systems and between different correlationIds and should be only created if none exists
   * Format is UUID V4
   */
  id: string;

  /**
   * Unique identifier for interaction category
   * An interaction category is for example: health insurance
   * Format is UUID V4
   */
  categoryId: string;
}
