import { ReplyDataType } from "./reply-data.type";
import { MessageTypeEnum } from "../message";

/**
 * When a user sends us a message from any source the interaction id will not be known. An interaction id must be assigned.
 */
export interface InteractionUnassignedReplyDataInterface<
  T extends ReplyDataType
> {
  source: T;
  text: string;
  messageId: string;
  type: MessageTypeEnum;
}
