import { PhoneSourceInterface } from "./phone-source.interface";
import { PhoneMmsDevice } from "../../phone";

export interface MmsSourceInterface
  extends PhoneSourceInterface<PhoneMmsDevice> {}
