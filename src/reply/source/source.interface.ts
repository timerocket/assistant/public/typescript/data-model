import { SourceDestinationDeviceType } from "../../source-destination-device.type";

export interface SourceInterface {
  device: SourceDestinationDeviceType;
}
