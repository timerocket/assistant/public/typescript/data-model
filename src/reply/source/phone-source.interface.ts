import { SourceInterface } from "./source.interface";
import { PhoneDevice } from "../../phone";
import { PhoneNumberChannelEnum } from "./phone-number-channel.enum";

export interface PhoneSourceInterface<T extends PhoneDevice>
  extends SourceInterface {
  device: T;
  sentToPhoneNumber: string;
  channel: PhoneNumberChannelEnum;
}
