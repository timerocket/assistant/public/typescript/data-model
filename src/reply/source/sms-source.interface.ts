import { PhoneSourceInterface } from "./phone-source.interface";
import { PhoneSmsDevice } from "../../phone";

export interface SmsSourceInterface
  extends PhoneSourceInterface<PhoneSmsDevice> {}
