import { MessageTypeEnum } from "../message";
import { AppMessageDataType } from "../app";

export interface SupportUserCommunication {
  fromUser: string;
  toUser: string;
  fromDevice: string;
  toDevice: string;
  id: string;
  text: string;
  type: MessageTypeEnum;
  createdAt: string;
  data?: AppMessageDataType;
}
