import { AuthenticationProviderInterface } from "./authentication-provider.interface";
import { AuthenticationProviderTypeEnum } from "./authentication-provider-type.enum";

export interface RefreshAuthAuthenticationProviderInterface
  extends AuthenticationProviderInterface {
  data: {
    token: string;
  };
  type: AuthenticationProviderTypeEnum.REFRESH;
}
