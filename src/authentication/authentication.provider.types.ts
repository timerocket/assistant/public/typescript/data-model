import { BasicAuthAuthenticationProviderInterface } from "./basic-auth.authentication-provider.interface";
import { RefreshAuthAuthenticationProviderInterface } from "./refresh.authentication-provider.interface";

export declare type AuthenticationProviderTypes = BasicAuthAuthenticationProviderInterface;
export declare type CreateTokenAuthenticationProviderTypes =
  | BasicAuthAuthenticationProviderInterface
  | RefreshAuthAuthenticationProviderInterface;
