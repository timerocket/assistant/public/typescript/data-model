export enum AuthenticationProviderTypeEnum {
  BASIC = "basic",
  REFRESH = "refresh",
}
