import { SourceDestinationTypeEnum } from "./source-destination-type.enum";

export interface Device {
  type: SourceDestinationTypeEnum;
}
