import { AttachmentTypeEnum } from "./attachment-type.enum";

export interface AttachmentInterface {
  type: AttachmentTypeEnum;
}
