import { AttachmentInterface } from "./attachment.interface";
import { AttachmentTypeEnum } from "./attachment-type.enum";

export interface VideoAttachmentInterface extends AttachmentInterface {
  type: AttachmentTypeEnum.VIDEO;
}
