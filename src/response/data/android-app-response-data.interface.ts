import { ResponseDataInterface } from "./response-data.interface";

export interface AndroidAppResponseDataInterface
  extends ResponseDataInterface {}
