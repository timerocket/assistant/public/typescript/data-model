import { CallToActionInterface } from "./call-to-action.interface";

export interface EmailCallToActionInterface extends CallToActionInterface {
  text: string;
  link: string;
}
