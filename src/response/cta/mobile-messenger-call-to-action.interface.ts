import { CallToActionInterface } from "./call-to-action.interface";

export interface MobileMessengerCallToActionInterface
  extends CallToActionInterface {
  text: string;
  link: string;
}
