import { AppMessageReplyInterface } from "./app-message-reply.interface";
import { AppMessageResponseInterface } from "./app-message-response.interface";
import { AppCardType } from "./card";
import { AppMessageDataType } from "./app-message-data.type";

export declare type CommunicationType =
  | AppMessageReplyInterface
  | AppMessageResponseInterface<AppMessageDataType>;
