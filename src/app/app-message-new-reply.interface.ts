import { AppMessageInterface } from "./app-message.interface";
import { AppDevice } from "./app-device";

export interface AppMessageNewReplyInterface extends AppMessageInterface {
  device: AppDevice;
  user: {
    id: 2;
  };
}
