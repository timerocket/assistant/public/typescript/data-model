import { AppCardType, AppMessageCardInterface } from "./card";

export declare type AppMessageDataType =
  | AppMessageCardInterface<AppCardType>
  | AppMessageCardInterface<AppCardType>[];
