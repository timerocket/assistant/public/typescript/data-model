import { SourceDestinationTypeEnum } from "../source-destination-type.enum";
import { Device } from "../device";
import { AppDeviceType } from "./app-device-type";

export interface AppDevice extends Device {
  /**
   * Unique device id for android or ios
   */
  id: string;

  type: AppDeviceType;
}
