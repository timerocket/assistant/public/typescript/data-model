import { AppMessageCardTypeEnum } from "./app-message-card-type-enum";
import { AppCardType } from "./app-card-type";

export interface AppMessageCardInterface<T extends AppCardType> {
  id: string;
  data: T;
  type: AppMessageCardTypeEnum;
}
