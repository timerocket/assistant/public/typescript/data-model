import { SavedCardInterface } from "./app-message-card-saved.interface";

export interface AppCardBenefitSessionInterface extends SavedCardInterface {
  title: string;
  description: string;
  sessionCount: number;
  sessionsComplete: number;
}
