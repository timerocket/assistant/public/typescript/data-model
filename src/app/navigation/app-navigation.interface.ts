import { AppNavigationComponentEnum } from "./app-navigation.component.enum";
import { AppNavigationNameEnum } from "./app-navigation.name.enum";

export interface AppNavigationInterface {
  title: string;
  name: AppNavigationNameEnum;
  component: AppNavigationComponentEnum;
}
