import { AppDevice } from "./app-device";
import { SourceDestinationTypeEnum } from "../source-destination-type.enum";

export interface AppAndroidDevice extends AppDevice {
  type: SourceDestinationTypeEnum.APP_ANDROID;
}
