import { AppCardType } from "./card";
import { AppMessageInterface } from "./app-message.interface";
import { AppMessageDataType } from "./app-message-data.type";

export interface AppMessageResponseInterface<T extends AppMessageDataType>
  extends AppMessageInterface {
  data?: AppMessageDataType;
  user: {
    id: 1;
  };
}
