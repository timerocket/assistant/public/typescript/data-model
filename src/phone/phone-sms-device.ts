import { PhoneDevice } from "./phone-device";
import { SourceDestinationTypeEnum } from "../source-destination-type.enum";

export interface PhoneSmsDevice extends PhoneDevice {
  type: SourceDestinationTypeEnum.PHONE_SMS;
}
