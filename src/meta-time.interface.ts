export interface MetaTimeInterface {
  /**
   * UTC timestamp in ISO 8601 format for when the correlation id was created
   * This timestamp should be passed between systems and should be only created if none exists
   */
  started: string;

  /**
   * UTC timestamp in ISO 8601 format for when this particular message was created
   */
  created: string;
}
